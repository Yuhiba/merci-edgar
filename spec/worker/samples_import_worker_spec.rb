require 'rails_helper'

describe SamplesImportWorker do

  it 'respond to #perform_async' do
    expect(SamplesImportWorker).to respond_to(:perform_async)
  end

  it "can perform" do
    SamplesImportWorker.any_instance.stubs(:import_sample_data) { true }
    expect {
      SamplesImportWorker.new.perform("1")
    }.to_not raise_error
  end

  it "works" do
    SamplesImportWorker.any_instance.stubs(:at) { true }

    show_buyer = FactoryGirl.create(:show_buyer)
    show_buyer.stubs(:update_attributes!)
    scheduling = FactoryGirl.create(:scheduling, show_buyer: show_buyer)
    festival = FactoryGirl.create(:festival, schedulings: [scheduling])
    Festival.stubs(:create!).returns(festival)

    person = FactoryGirl.create(:person)
    person.stubs(:update_attributes!)
    structure = FactoryGirl.create(:structure, people: [person])
    Structure.stubs(:create!).returns(structure)

    venue = FactoryGirl.create(:venue, structure: structure)
    Venue.stubs(:create!).returns(venue)

    other_person = FactoryGirl.create(:person)
    Person.stubs(:create!).returns(other_person)

    expect {
      SamplesImportWorker.new.import_sample_data
    }.to_not raise_error
  end

end
