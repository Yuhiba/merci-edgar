require "rails_helper"
require "tempfile"

describe ExportContacts do

  describe "#perform" do
    it "can perform" do
      account = FactoryGirl.create(:account)
      manager = FactoryGirl.create(:user, manager: true)
      ExportContacts.any_instance.stubs(:store) { true }
      mailer = Object.new
      mailer.stubs(:deliver) { }
      UserMailer.stubs(:export_contact) { mailer }
      expect {
        ExportContacts.new.perform(account.id, manager.id)
      }.to_not raise_error
    end
  end

  describe "#store" do
    it "works" do
      file = Tempfile.new('foo')
      ExportUploader.any_instance.stubs(:store!)
      expect {
        ExportContacts.new.store(file)
      }.to_not raise_error
    end
  end
end
