require 'rails_helper'

describe SubscriptionsController do
  describe "#new" do
    it "doesnt work with http protocol" do
      user = FactoryGirl.create(:admin)
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in user
      @request.env['HTTPS'] = 'off'
      get 'new'
      expect(response).not_to be_success
    end

    it "works with https protocol" do
      user = FactoryGirl.create(:admin)
      @request.env["devise.mapping"] = Devise.mappings[:user]
      @request.env['HTTPS'] = 'on'
      sign_in user
      get 'new'
      expect(response).to be_success
    end
  end

  describe "#edit" do
    it "works" do
      user = FactoryGirl.create(:admin)
      @request.env["devise.mapping"] = Devise.mappings[:user]
      @request.env['HTTPS'] = 'on'
      sign_in user
      get 'edit'
      expect(response).to be_success
    end
  end

  describe "#update" do
    it "works" do
      user = FactoryGirl.create(:admin)
      @request.env["devise.mapping"] = Devise.mappings[:user]
      @request.env['HTTPS'] = 'on'
      sign_in user

      Stripe::Charge.stubs(:create)
      SendUpgradeReceiptEmailWorker.stubs(:perform_async)

      post 'update'

      expect(response).to be_redirect
    end
  end

  describe "#create" do
    it "works" do
      user = FactoryGirl.create(:admin)
      @request.env["devise.mapping"] = Devise.mappings[:user]
      @request.env['HTTPS'] = 'on'
      sign_in user

      Stripe::Charge.stubs(:create)
      SendUpgradeReceiptEmailWorker.stubs(:perform_async)

      post 'create', team: 'true'

      expect(response).to be_redirect
    end
  end


end
