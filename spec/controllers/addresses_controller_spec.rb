require "rails_helper"

describe AddressesController do
  describe "#update" do
    it "works" do
      account = FactoryGirl.create(:account)
      address = FactoryGirl.create(:address, account: account)
      manager = FactoryGirl.create(:user, account: account, manager: true)
      @request.host = "#{account.domain}.lvh.me"
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in manager

      expect(address.reload.geocoded_precisely).to be_falsy
      post :update, id: address.id, format: 'json'
      expect(response).to be_success
      expect(address.reload.geocoded_precisely).to be_truthy
      expect(response.body).to eq("{\"lat\":null,\"lng\":null,\"yupee\":\"ok\"}")
    end
  end
end
