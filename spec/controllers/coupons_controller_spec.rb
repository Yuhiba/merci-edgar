require "rails_helper"

describe CouponsController do

  describe "#index" do
    it "works well with a manager with the account assigns" do
      account = FactoryGirl.create(:account)
      manager = FactoryGirl.create(:user, account: account, manager: true)
      @request.host = "#{account.domain}.lvh.me"
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in manager

      get :index

      expect(response).to be_success
    end
  end

  describe "#new" do
    it "works well with a manager with the account assigns" do
      account = FactoryGirl.create(:account)
      manager = FactoryGirl.create(:user, account: account, manager: true)
      @request.host = "#{account.domain}.lvh.me"
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in manager

      get :new

      expect(response).to be_success
    end
  end

  describe "#create" do
    it "works well with a manager with the account assigns" do
      account = FactoryGirl.create(:account)
      manager = FactoryGirl.create(:user, account: account, manager: true)
      @request.host = "#{account.domain}.lvh.me"
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in manager

      post :create

      expect(response).to be_redirect
    end
  end

  describe "#update" do
    it "works well with a manager with the account assigns" do
      account = FactoryGirl.create(:account)
      manager = FactoryGirl.create(:user, account: account, manager: true)
      @request.host = "#{account.domain}.lvh.me"
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in manager
      coupon = FactoryGirl.create(:coupon)

      post :update, id: coupon.id, format: 'js'

      expect(response).to be_success
    end
  end


end
