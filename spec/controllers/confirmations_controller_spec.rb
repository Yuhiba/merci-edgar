require "rails_helper"

describe ConfirmationsController do

  describe "#create" do
    it "works" do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      post :create
      expect(response).to be_success
    end
  end

  describe "#update" do
    it "works" do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      post :update
      expect(response).to be_success
    end


    it "works with confirmation token" do
      user = FactoryGirl.create(:user, confirmation_token: 'qwerty', confirmed_at: Time.now)
      @request.env["devise.mapping"] = Devise.mappings[:user]
      post :update, confirmation_token: 'qwerty'
      expect(response).to be_success
    end
  end

  describe "#show" do
    it "works" do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      get :show
      expect(response).to be_success
    end
  end


end

