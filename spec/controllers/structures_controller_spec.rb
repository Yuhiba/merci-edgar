require 'rails_helper'

describe StructuresController do

  describe "#index" do
    it "works" do
      account = FactoryGirl.create(:account)
      manager = FactoryGirl.create(:user, account: account, manager: true)
      @request.host = "#{account.domain}.lvh.me"
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in manager

      get :index, term: "laclef"

      expect(response).to be_success
    end
  end

  describe "#new" do
    it "works" do
      account = FactoryGirl.create(:account)
      manager = FactoryGirl.create(:user, account: account, manager: true)
      @request.host = "#{account.domain}.lvh.me"
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in manager

      get :new

      expect(response).to be_success
    end
  end

  describe "#create" do
    it "works" do
      account = FactoryGirl.create(:account)
      manager = FactoryGirl.create(:user, account: account, manager: true)
      @request.host = "#{account.domain}.lvh.me"
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in manager

      attr = {"structure"=>{"kind"=>"structure", "contact_attributes"=>{"name"=>"toto", "addresses_attributes"=>{"0"=>{"city"=>"", "postal_code"=>"", "country"=>"FR"}}}}}

      post :create, attr

      expect(response).to be_redirect
    end
  end

  describe "#edit" do
    it "works" do
      account = FactoryGirl.create(:account)
      manager = FactoryGirl.create(:user, account: account, manager: true)
      contact = FactoryGirl.create(:contact, account_id: account.id)
      structure = FactoryGirl.create(:structure, account_id: account.id, contact: contact)
      Account.current_id = account.id
      @request.host = "#{account.domain}.lvh.me"
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in manager

      get :edit, id: structure.id

      expect(response).to be_success
    end
  end

  describe "#update" do
    it "works" do
      account = FactoryGirl.create(:account)
      contact = FactoryGirl.create(:contact, account_id: account.id)
      structure = FactoryGirl.create(:structure, account_id: account.id, contact: contact)
      manager = FactoryGirl.create(:user, account: account, manager: true)
      @request.host = "#{account.domain}.lvh.me"
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in manager

      post :update, id: structure.id, structure: {contact_attributes: {"name"=>"other"}}

      expect(response).to be_redirect
    end
  end

  describe "#show" do
    it "works" do
      account = FactoryGirl.create(:account)
      manager = FactoryGirl.create(:user, account: account, manager: true)
      contact = FactoryGirl.create(:contact, account_id: account.id)
      structure = FactoryGirl.create(:structure, account_id: account.id, contact: contact)
      Account.current_id = account.id
      @request.host = "#{account.domain}.lvh.me"
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in manager

      get :show, id: structure.id

      expect(response).to be_success
    end
  end

  describe "#destroy" do
    it "works" do
      account = FactoryGirl.create(:account)
      manager = FactoryGirl.create(:user, account: account, manager: true)
      contact = FactoryGirl.create(:contact, account_id: account.id)
      structure = FactoryGirl.create(:structure, account_id: account.id, contact: contact)
      Account.current_id = account.id
      @request.host = "#{account.domain}.lvh.me"
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in manager

      post :destroy, id: structure.id

      expect(response).to be_redirect
    end
  end

  describe "#set_main_person" do
    it "works" do
      account = FactoryGirl.create(:account)
      manager = FactoryGirl.create(:user, account: account, manager: true)
      contact = FactoryGirl.create(:contact, account_id: account.id)
      person = FactoryGirl.create(:person, account_id: account.id)
      structure = FactoryGirl.create(:structure, account_id: account.id, contact: contact)
      Account.current_id = account.id
      @request.host = "#{account.domain}.lvh.me"
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in manager

      put :set_main_person, id: person.id, structure_id: structure.id, format: 'js'

      expect(response).to be_success
    end
  end

end
