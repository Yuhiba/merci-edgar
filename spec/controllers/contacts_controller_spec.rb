require 'rails_helper'

describe ContactsController do
  describe "#index" do
    it "without existing contact linked to account return empty template" do
      account = FactoryGirl.create(:account)
      user = FactoryGirl.create(:user, account: account)
      @request.host = "#{account.domain}.lvh.me"
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in user

      get 'index'
      expect(response).to be_success
      expect(response).to render_template('empty')
    end

    it "works" do
      account = FactoryGirl.create(:account)
      user = FactoryGirl.create(:user, account: account)
      current_account = account
      FactoryGirl.create(:contact, account: account)
      @request.host = "#{account.domain}.lvh.me"
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in user

      get 'index'
      expect(response).to be_success
      expect(response).to_not render_template('empty')
    end
  end

  describe "#autocomplete" do
    it "works" do
      account = FactoryGirl.create(:account)
      user = FactoryGirl.create(:user, account: account)
      @request.host = "#{account.domain}.lvh.me"
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in user

      get 'autocomplete', term: 'bac'
      expect(Contact.count).to eq(0)
      expect(response).to be_success
      expect(response.body).to eq([{value:"bac",label:"Cr\u00e9er la structure : bac", new:"true",link:"/fr/structures/new?name=bac"},{value:"bac",label:"Cr\u00e9er la personne : bac",new:"true",link:"/fr/people/new?name=bac"}].to_json)
    end
  end

  describe "#only" do
    it "works" do
      account = FactoryGirl.create(:account)
      user = FactoryGirl.create(:user, account: account)
      @request.host = "#{account.domain}.lvh.me"
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in user

      get 'only', filter: 'notexisting'
      expect(response).to redirect_to(contacts_path)
    end

    FILTERS = %w(favorites contacted recently_created recently_updated style network custom contract dept capacities_less_than capacities_more_than capacities_between)
    FILTERS.each do |filter|
      it "works with filter #{filter}" do
        account = FactoryGirl.create(:account)
        user = FactoryGirl.create(:user, account: account)
        @request.host = "#{account.domain}.lvh.me"
        @request.env["devise.mapping"] = Devise.mappings[:user]
        sign_in user

        get 'only', filter: filter
        expect(response).to be_success
      end
    end
  end

  describe "#bulk" do

    it "works only with a bulk_action" do
      account = FactoryGirl.create(:account)
      user = FactoryGirl.create(:user, account: account)
      contact = FactoryGirl.create(:contact, account: account)
      @request.host = "#{account.domain}.lvh.me"
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in user

      get :bulk, format: 'js', bulk_action: 'add_custom_tags', bulk_value: 'some tags', contact_ids: contact.id

      expect(response).to be_success
    end
  end

  describe "#add_to_favorites" do
    it "works" do
      account = FactoryGirl.create(:account)
      user = FactoryGirl.create(:user, account: account)
      contact = FactoryGirl.create(:contact, account: account)
      @request.host = "#{account.domain}.lvh.me"
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in user

      get :add_to_favorites, id: contact.id, format: 'js'

      expect(response).to be_success
    end
  end

  describe "#remove_to_favorites" do
    it "works" do
      account = FactoryGirl.create(:account)
      user = FactoryGirl.create(:user, account: account)
      contact = FactoryGirl.create(:contact, account: account)
      @request.host = "#{account.domain}.lvh.me"
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in user

      get :remove_to_favorites, id: contact.id, format: 'js'

      expect(response).to be_success
    end
  end

  describe "#show_map" do
    it "works" do
      account = FactoryGirl.create(:account)
      user = FactoryGirl.create(:user, account: account)
      contact = FactoryGirl.create(:contact, account: account)
      @request.host = "#{account.domain}.lvh.me"
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in user

      get :show_map, id: contact.id, format: 'js'

      expect(response).to be_success
    end
  end

end
