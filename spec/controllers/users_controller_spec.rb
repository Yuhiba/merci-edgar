require 'rails_helper'

describe UsersController do

  describe "#show" do

    it "works" do
      user = FactoryGirl.create(:user)
      sign_in user

      get :show, :id => user.id

      expect(response).to be_success
      expect(assigns(:user)).to eq(user)
    end

  end

  describe "#index" do

    it "works" do
      user = FactoryGirl.create(:admin)
      sign_in user

      get :index

      expect(response).to be_success
    end

  end

  describe "#edit" do

    it "works" do
      user = FactoryGirl.create(:user)
      sign_in user

      get :edit, :id => user.id

      expect(response).to be_success
    end

  end

  describe "#update" do

    it "current user" do
      user = FactoryGirl.create(:user)
      sign_in user

      post :update, :id => user.id

      expect(response).to be_redirect
    end

    it "other user if admin" do
      admin = FactoryGirl.create(:admin)
      user = FactoryGirl.create(:user)
      sign_in admin

      post :update, :id => user.id

      expect(response).to be_redirect
    end

  end

  describe "#destroy" do

    it "works" do
      admin = FactoryGirl.create(:admin)
      user = FactoryGirl.create(:user)
      sign_in admin

      post :destroy, :id => user.id

      expect(response).to be_redirect
    end

  end

  describe "#invite" do

    it "works" do
      request.env["HTTP_REFERER"] = root_path
      user = FactoryGirl.create(:admin)
      sign_in user

      post :invite, :id => user.id

      expect(response).to be_redirect
    end

  end

  describe "#bulk_invite" do

    it "works" do
      request.env["HTTP_REFERER"] = root_path
      user = FactoryGirl.create(:admin)
      sign_in user

      get :bulk_invite, quantity: 1

      expect(response).to be_redirect
    end

  end


end
