require "rails_helper"

describe ContactsImportsController do

  describe "#new" do
    it "works well with a manager with the account assigns" do
      account = FactoryGirl.create(:account)
      manager = FactoryGirl.create(:user, account: account, manager: true)
      @request.host = "#{account.domain}.lvh.me"
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in manager

      get :new

      expect(response).to be_success
      expect(assigns(:current_account)).to eq(account)
    end
  end

  describe "#new_with_details'" do
    it "works well with a manager with the account assigns" do
      account = FactoryGirl.create(:account)
      manager = FactoryGirl.create(:user, account: account, manager: true)
      @request.host = "#{account.domain}.lvh.me"
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in manager

      get :new_with_details, key: 'something'

      expect(response).to be_success
      expect(assigns(:current_account)).to eq(account)
      expect(assigns(:import)).to_not be_nil
    end
  end

  describe "#create" do
    it "works well with a manager with the account assigns" do
      account = FactoryGirl.create(:account)
      manager = FactoryGirl.create(:user, account: account, manager: true)
      @request.host = "#{account.domain}.lvh.me"
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in manager

      post :create

      expect(response).to be_success
    end
  end

  describe "#update" do
    it "works well with a manager with the account assigns" do
      account = FactoryGirl.create(:account)
      manager = FactoryGirl.create(:user, account: account, manager: true)
      contacts_import = FactoryGirl.create(:contacts_import, account_id: account.id, user_id: manager.id)
      @request.host = "#{account.domain}.lvh.me"
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in manager

      post :update, id: contacts_import.id

      expect(response).to be_success
    end
  end


end
